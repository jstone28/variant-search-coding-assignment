"""vsca setup"""
from flask import Flask
from flask_cors import CORS
from api.v1.routes import v1

vsca = Flask(__name__)
CORS(vsca)
vsca.register_blueprint(v1, url_prefix="/v1")


@vsca.route("/")
def index():
  return "Current API version is v1. Prefix your call with /v1"


@vsca.route("/version")
def get_version():
    """ returns application version"""
    with open("./VERSION", "r") as version:
        version = version.read()
    return str("Application Version: v" + version)
