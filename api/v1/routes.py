"""v1 routes"""

from flask import Blueprint, request
from api.v1.db import db

v1 = Blueprint("v1", __name__)


@v1.route("/")
def root():
    return "available routes: [\"/gene\"]"

@v1.route("/gene")
def gene_route():
    return db.gene_search()
