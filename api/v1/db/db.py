"""db handles connection with elasticsearch"""
from elasticsearch import Elasticsearch
from flask_restful import reqparse
import json
import os

parser = reqparse.RequestParser()

# get the HOST from the env or default to localhost
HOST = os.getenv("TRS_HOST", "http://localhost:9200")

# instantiate elastic search with HOST
es = Elasticsearch([HOST])


def gene_search():
    """search es based on gene_name"""
    parser.add_argument("q")
    query_string = parser.parse_args()
    if query_string["q"] == None:
        term = {"query": {"match_all": {}}}
    else:
        term = {
            "query": {
                "multi_match": {
                    "fields": ["Gene"],
                    "query": query_string['q'],
                    "type": "cross_fields",
                    "use_dis_max": False
                }
            },
        }

    results = es.search(
        index="gene-variants",
        body=term,
        size=1000
    )

    gene = []
    for hit in results["hits"]["hits"]:
        gene.append(hit["_source"])
    search = {"results": gene}
    return json.dumps(search)
