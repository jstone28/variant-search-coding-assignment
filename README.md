# VSCA

[V]ariant [S]earch [C]oding [A]ssignment

## Assignment

> Create a web application that allows a user to search for genomic variants by gene name and display the results in a tabular view.

More information about project requirements can be found in the [Assignment](./ASSIGNMENT.md) doc.

## Installing

## Getting Started

## Specification and Discussion

Discussion regarding implementation can be found in the [specification](./docs/SPEC.md).
