
## 0.0.5 [11-08-2019]

* Bug fixes and performance improvements

See commit c874388

---

## 0.0.4 [11-08-2019]

* Bug fixes and performance improvements

See commit da96d26

---

## 0.0.3 [11-07-2019]

* Bug fixes and performance improvements

See commit 23770e5

---

## 0.0.2 [11-07-2019]

* Bug fixes and performance improvements

See commit 283fa7b

---
