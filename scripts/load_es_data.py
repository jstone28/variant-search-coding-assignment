#!/usr/bin/env python
"""Loads TSV data converted to JSON and pushes it into Elasticsearch"""

from elasticsearch import helpers, Elasticsearch
import json
import os
import csv

# look for env var host or default to localhost
HOST = os.getenv("GENE_VARIANTS_HOST", "http://localhost:9200")
es = Elasticsearch([HOST])
id = 0

def create_index():
    settings = {
        "settings": {
            "analysis": {
            "filter": {
                "autocomplete_filter": {
                "type": "edge_ngram",
                "min_gram": 1,
                "max_gram": 20
                }
            },
            "analyzer": {
                "autocomplete": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": [
                    "lowercase",
                    "autocomplete_filter"
                ]
                }
            }
            }
        },
        "mappings": {
            "members": {
                "properties": {
                    "Gene": {
                        "type": "text",
                        "analyzer": "autocomplete",
                        "search_analyzer": "standard"
                    }
                }
            }
        }
    }

    es.indices.create(index="gene-variants", body=settings)
    print("Created Index")


create_index()


# open the json variants file
with open('./examples/data/variants.json', 'r') as data:
    json_data = data.read()
    # this is a list of json objects (examples/data/variants.json)
    gene_data = json.loads(json_data)
    for gdata in gene_data:
      # loop over the objects and them into docs within ES
      id+=1 # increment the id
      print("adding " + str(id))
      es.create(index="gene-variants", body=gdata, id=id)
