# VSCA

[V]ariant [S]earch [C]oding [A]ssignment

## Overview

![v1](https://gitlab.com/jstone28/variant-search-coding-assignment/uploads/4b6b7de6a560ecb5c9d10cf18a929daa/image.png)

Quick Notes on architecture:

* We're using Gitlab because they offer a free tier that includes 2k free pipeline minutes. The pipelines exist as the backbone to our CI/CD process for this application.
* The primary feature set required of the assignment is search, we'll use ElasticSearch. ElasticSearch makes querying data super simple and, in fact, gives you a RESTful API out-of-the-box, however, we'll implement an API in front of ElasticSearch to allow for additional programmability.
* We have coupled the frontend and backend into the same repository for this exercise. If this were a production application, I may or may not change that based on the team and other architectural decisions across the greater platform. The reason for separating them is to allow for independent development of frontend or backend (depending on if one changes more than the other)

## Tools:

* Frontend: [Vue](https://vuejs.org/) with [Buefy](https://buefy.org/)
* Backend: Python with Flask
* Database: ElasticSearch

* Hosted: GKE
* Container runtime: Containerd/Docker
* Build Pipelines: Gitlab Runner

## Assumptions

* The process by which data moves into ElasticSearch is a static process. This is because the ask is for a search mechanism and accompanying UI, and not a data reconciliation process. Nonetheless, the static process is outlined in `scripts/load_es_data.py`
