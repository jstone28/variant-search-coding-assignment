FROM node:12
WORKDIR /app
COPY web .
RUN npm install && npm run build

FROM nginx
RUN mkdir /app
COPY --from=0 /app/dist /app
COPY web/conf/nginx.conf /etc/nginx/nginx.conf
