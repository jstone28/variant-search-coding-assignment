run-flask:
	export FLASK_APP=runserver.py; flask run

run:
	gunicorn --bind 0.0.0.0:5000 --workers 2 --threads 2 --timeout 120 --log-level debug runserver:vsca

frontend-dev:
	cd web && npm run serve

install:
	cd web && npm install

lint:
	cd web && npm run lint

test:
	cd web && npm test

docker-build:
	docker build -t vsca .

docker-run:
	docker run -p 8080:80 -p 3030:3030 -p 9200:9200 vsca

migrate-data:
	./scripts/migrate-data
